﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace ForexWebApp
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NewsJson" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select NewsJson.svc or NewsJson.svc.cs at the Solution Explorer and start debugging.
	[ServiceContract]
	public class NewsJson 
	{
		public static string _newsCache = HttpContext.Current.Server.MapPath("~/News.json");
		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Json,
			UriTemplate = "News")]
		public List<NewsItem> GetNews()
		{
			try
			{
				return JsonConvert.DeserializeObject<List<NewsItem>>(File.ReadAllText(_newsCache));
			}
			catch
			{
				return new List<NewsItem>();
			}
		}
		public static string _pageCache = HttpContext.Current.Server.MapPath("~/Page.json");
		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Json,
			UriTemplate = "Page")]
		public string GetPage()
		{
			try
			{
				return File.ReadAllText(_pageCache);
			}
			catch
			{
				return "";
			}
		}
	}

}
