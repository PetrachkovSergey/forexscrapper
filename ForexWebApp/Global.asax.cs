﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace ForexWebApp
{
	public class Global : System.Web.HttpApplication
	{
		//private static string _url = "http://www.forexpf.ru/chart/usdrub/";
		//public static  string _newsCache = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/News.json");
		//private static string _cachedPage =  System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Page.json");
		//private static readonly string _forexLinkFirstPart = @"http://www.forexpf.ru";
		//protected void Application_Start(object sender, EventArgs e)
		//{
		//	try
		//	{
		//		Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.Idle; 
		//		new Thread(delegate()
		//		{
					
		//			DoScrapping();
		//		}).Start();
		//	}
		//	catch (Exception ex)
		//	{
		//		Console.WriteLine(ex.Message);
		//	}
		//}
		//private void DoScrapping()
		//{
		//	while (true)
		//	{
		//		try
		//		{				
		//			bool isFileExists = File.Exists(_newsCache);
		//			if (!isFileExists)
		//			{
		//				CollectAndSaveData();
		//			}
		//			else
		//			{
		//				DateTime lastChangeTime = File.GetLastWriteTime(_newsCache);
		//				TimeSpan timespan = DateTime.Now - lastChangeTime;
		//				if (timespan.TotalMinutes > 1) 
		//				{
		//					Stopwatch sw = new Stopwatch();
		//					Trace.TraceInformation("start scrapping on: {0}", DateTime.Now);
		//					sw.Start();
		//					CollectAndSaveData();
		//					sw.Stop();
		//					Trace.TraceInformation("scrapping completed. spent time:{0}", sw.Elapsed);
		//				}
		//				else
		//				{
		//					Thread.Sleep(10000);
		//				}
		//			}
		//		}
		//		catch (ThreadAbortException abrtEx)
		//		{
		//			break;
		//		}
		//		catch (Exception ex)
		//		{
		//			Thread.Sleep(10000);
		//		}
		//	}
		//}

		//private static void CollectAndSaveData()
		//{
		//	string pageHtml = "";
		//	var news = ParseNews(out pageHtml);
		//	if (!string.IsNullOrWhiteSpace(pageHtml))
		//	{
		//		SaveNewsAndPageHtml(news, pageHtml);
		//	}
		//}
		//private static List<NewsItem> ParseNews(out string pageHtml)
		//{
		//	var result = new List<NewsItem>();
		//	HtmlWeb htmlWeb = new HtmlWeb();
		//	HtmlDocument document = htmlWeb.Load(_url);

		//	var newsRows = document.DocumentNode.Descendants()
		//		.Where(x => x.Name == "div" && x.Attributes.Contains("class")
		//		&& x.Attributes["class"].Value.Split().Contains("pfs-row"));

		//	foreach (var item in newsRows)
		//	{
		//		var newsItem = new NewsItem()
		//		{
		//			Date = item.Descendants().
		//			Where(x => x.Name == "div" && x.Attributes.Contains("class") && x.Attributes["class"].Value.Split().Contains("pfs-time")).
		//			Single().
		//			InnerText.
		//			Replace("\n", "").
		//			Replace("\t", ""),

		//			Title = item.Descendants().
		//			Where(x => x.Name == "div" && x.Attributes.Contains("class") && x.Attributes["class"].Value.Split().Contains("pfs-title")).
		//			Single().
		//			InnerText.
		//			Replace("\n", "").
		//			Replace("\t", ""),

		//			Url = item.Descendants().
		//			Where(x => x.Name == "div" && x.Attributes.Contains("class") && x.Attributes["class"].Value.Split().Contains("pfs-title")).
		//			Single().
		//			Descendants().
		//			Where(x => x.Name == "a" && x.Attributes.Contains("href")).Single().Attributes["href"].Value.Split().Single(),
		//			Content = ""
		//		};
		//		try
		//		{
		//			HtmlDocument itemDoc = new HtmlDocument();
		//			itemDoc.OptionCheckSyntax = true;
		//			itemDoc.OptionFixNestedTags = true;
		//			itemDoc.OptionAutoCloseOnEnd = true;
		//			itemDoc.LoadHtml((new HtmlWeb().Load(newsItem.Url)).DocumentNode.OuterHtml);	
					
		//			var blogCode = itemDoc.DocumentNode.Descendants().
		//				Where(x => x.Name == "a" && x.Attributes.Contains("class") && x.Attributes["class"].Value.Split().Contains("blogButton")).SingleOrDefault();
		//			if (blogCode != null)
		//			{
		//				var onclickString = blogCode.Attributes.Where(x => x.Name == "onclick").SingleOrDefault();
		//				var partialLink = onclickString.Value.Split().Where(x=>x.Contains(".html")).SingleOrDefault().Split(new char[]{'\''}).Where(x=>x.Contains(".html")).SingleOrDefault();
		//				if (string.IsNullOrWhiteSpace(partialLink))
		//					continue;
		//				var fullLink = "" + _forexLinkFirstPart + partialLink;
		//				var newsDoc = new HtmlDocument();
		//				newsDoc.OptionCheckSyntax = true;
		//				newsDoc.OptionFixNestedTags = true;
		//				newsDoc.OptionAutoCloseOnEnd = true;
		//				newsDoc.OptionWriteEmptyNodes = true;
						
		//				newsDoc.LoadHtml((new HtmlWeb().Load(fullLink)).DocumentNode.OuterHtml);
						
		//				var newsCode = newsDoc.DocumentNode.Descendants().
		//					Where(x => x.Name == "textarea" && x.Attributes.Contains("name") && x.Attributes["name"].Value.Split().Contains("html")).SingleOrDefault();
		//				if (newsCode != null)
		//					newsItem.Content = newsCode.InnerHtml;
		//			}
		//		}
		//		catch(Exception ex)
		//		{
		//			Trace.TraceError("invalid html. {0}", ex);
		//			continue;
		//		}
		//		result.Add(newsItem);
				
		//	}
		//	pageHtml = document.DocumentNode.OuterHtml;
			
		//	return result;
		//}
		//private static bool SaveNewsAndPageHtml(List<NewsItem> news, string pageHtml)
		//{
		//	try
		//	{

		//		File.WriteAllText(_cachedPage, pageHtml);
		//		File.WriteAllText(_newsCache, JsonConvert.SerializeObject(news));
		//		return true;
		//	}
		//	catch
		//	{
		//		return false;
		//	}
		//}
		//protected void Session_Start(object sender, EventArgs e)
		//{

		//}

		//protected void Application_BeginRequest(object sender, EventArgs e)
		//{

		//}

		//protected void Application_AuthenticateRequest(object sender, EventArgs e)
		//{

		//}

		//protected void Application_Error(object sender, EventArgs e)
		//{
		//	Trace.TraceError("Ann error occured while app execution. Application_Error. EventArgs:{0}", e.ToString());
		//}

		//protected void Session_End(object sender, EventArgs e)
		//{
		//	Trace.TraceInformation("Session end. EventArgs{0}", e.ToString());
		//}

		//protected void Application_End(object sender, EventArgs e)
		//{
		//	Trace.TraceInformation("Application end. EventArgs{0}", e.ToString());
		//}
	}
}