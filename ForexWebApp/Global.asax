﻿<%@ Application  Inherits="ForexWebApp.Global" Language="C#" %>
<%@ Import Namespace="HtmlAgilityPack"%>
<%@ Import Namespace="Newtonsoft.Json"%> 
<%@ Import Namespace ="System" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="ForexWebApp" %>

<script RunAt="server">
	private static string _url = "http://www.forexpf.ru/chart/usdrub/";
	public static string _newsCache = HttpContext.Current.Server.MapPath("~/News.json");
	private static string _cachedPage = HttpContext.Current.Server.MapPath("~/Page.json");
	private static readonly string _forexLinkFirstPart = @"http://www.forexpf.ru";
	private readonly string regexHtmlPattern = @"https?:\/\/?([\da-z\.-]+\.[a-z\.]([\/\w \.-]*)*\/?)";
	private readonly string regexPartialHtmlPattern = "href=\"(.+)\"";
	private readonly string regexHrefPattern = "href=\"([\\/\\w+-\\/]*)\"";
	protected void Application_Start(object sender, EventArgs e)
	{
		try
		{
			//Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.Normal;
			new Thread(delegate()
			{
				DoScrapping();
			}).Start();
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex.Message);
		}
	}
	private void DoScrapping()
	{
		while (true)
		{
			try
			{
				bool isFileExists = File.Exists(_newsCache);
				if (!isFileExists)
				{
					CollectAndSaveData();
				}
				else
				{
					DateTime lastChangeTime = File.GetLastWriteTime(_newsCache);
					TimeSpan timespan = DateTime.Now - lastChangeTime;
					if (timespan.TotalMinutes > 30)
					{
						Stopwatch sw = new Stopwatch();
						Trace.TraceInformation("start scrapping on: {0}", DateTime.Now);
						sw.Start();
						CollectAndSaveData();
						sw.Stop();
						Trace.TraceInformation("scrapping completed. spent time:{0}", sw.Elapsed);
					}
					else
					{
						Thread.Sleep(10000);
					}
				}
			}
			catch (ThreadAbortException abrtEx)
			{
				break;
			}
			catch (Exception ex)
			{
				Thread.Sleep(10000);
			}
		}
	}

	private void CollectAndSaveData()
	{
		string pageHtml = "";
		var news = ParseNews(out pageHtml);
		if (!string.IsNullOrWhiteSpace(pageHtml))
		{
			SaveNewsAndPageHtml(news, pageHtml);
		}
	}
	private List<NewsItem> ParseNews(out string pageHtml)
	{
		var result = new List<NewsItem>();
		HtmlWeb htmlWeb = new HtmlWeb();
		HtmlDocument document = htmlWeb.Load(_url);

		var newsRows = document.DocumentNode.Descendants()
			.Where(x => x.Name == "div" && x.Attributes.Contains("class")
			&& x.Attributes["class"].Value.Split().Contains("pfs-row"));

		foreach (var item in newsRows)
		{
			var newsItem = new NewsItem()
			{
				Date = item.Descendants().
				Where(x => x.Name == "div" && x.Attributes.Contains("class") && x.Attributes["class"].Value.Split().Contains("pfs-time")).
				Single().
				InnerText.
				Replace("\n", "").
				Replace("\t", ""),

				Title = item.Descendants().
				Where(x => x.Name == "div" && x.Attributes.Contains("class") && x.Attributes["class"].Value.Split().Contains("pfs-title")).
				Single().
				InnerText.
				Replace("\n", "").
				Replace("\t", ""),

				Url = item.Descendants().
				Where(x => x.Name == "div" && x.Attributes.Contains("class") && x.Attributes["class"].Value.Split().Contains("pfs-title")).
				Single().
				Descendants().
				Where(x => x.Name == "a" && x.Attributes.Contains("href")).Single().Attributes["href"].Value.Split().Single(),
				Content = ""
			};
			try
			{
				HtmlDocument itemDoc = new HtmlDocument();
				itemDoc.OptionCheckSyntax = true;
				itemDoc.OptionFixNestedTags = true;
				itemDoc.OptionAutoCloseOnEnd = true;
				itemDoc.LoadHtml((new HtmlWeb().Load(newsItem.Url)).DocumentNode.OuterHtml);

				var blogCode = itemDoc.DocumentNode.Descendants().
					Where(x => x.Name == "a" && x.Attributes.Contains("class") && x.Attributes["class"].Value.Split().Contains("blogButton")).SingleOrDefault();
				if (blogCode != null)
				{
					var onclickString = blogCode.Attributes.Where(x => x.Name == "onclick").SingleOrDefault();
					var partialLink = onclickString.Value.Split().Where(x => x.Contains(".html")).SingleOrDefault().Split(new char[] { '\'' }).Where(x => x.Contains(".html")).SingleOrDefault();
					if (string.IsNullOrWhiteSpace(partialLink))
						continue;
					var fullLink = "" + _forexLinkFirstPart + partialLink;
					var newsDoc = new HtmlDocument();
					newsDoc.OptionCheckSyntax = true;
					newsDoc.OptionFixNestedTags = true;
					newsDoc.OptionAutoCloseOnEnd = true;
					newsDoc.OptionWriteEmptyNodes = true;

					newsDoc.LoadHtml((new HtmlWeb().Load(fullLink)).DocumentNode.OuterHtml);

					var newsCode = newsDoc.DocumentNode.Descendants().
						Where(x => x.Name == "textarea" && x.Attributes.Contains("name") && x.Attributes["name"].Value.Split().Contains("html")).SingleOrDefault();
					var innerHtml = newsCode.InnerHtml;
					var fixedHtml = FixHtmlFromString(innerHtml);
					var fixedImages = FixImagePaths(fixedHtml);
					newsItem.Content = fixedImages;
					
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError("invalid html. {0}", ex);
				continue;
			}
			result.Add(newsItem);

		}
		pageHtml = document.DocumentNode.OuterHtml;

		return result;
	}
	/// <summary>
	/// Makes all relative paths absolute
	/// </summary>
	/// <param name="sourceHtml">the source html code of your page</param>
	/// <returns>fixed html code</returns>
	public string FixHtmlFromString(string sourceHtml)
	{
		var matches = Regex.Matches(sourceHtml, regexHrefPattern, RegexOptions.IgnoreCase);
		string result = sourceHtml;
		foreach (Match match in matches)
		{
			var url = match.Groups[0].Value;
			if (url.Contains("www.forexpf.ru") || url == @"/")
				continue;
			var fixedUrl = "href = \"http://www.forexpf.ru" + match.Groups[1].Value+ "\"";
			result = "" + result.Replace(url, fixedUrl);
		}
		return result;
	}
	/// <summary>
	/// removes cache and icon size from image paths
	/// </summary>
	/// <param name="sourceHtml">the source html code of your page</param>
	/// <returns>fixed html code</returns>
	public string FixImagePaths(string sourceHtml)
	{
		var matches = Regex.Matches(sourceHtml, regexHtmlPattern, RegexOptions.IgnoreCase);
		string result = sourceHtml;
		foreach (Match match in matches)
		{
			var url = match.Groups[1].Value;
			if (url.Contains("/cache") && url.Contains("/128x128"))
			{
				var noCache = url.Replace("/cache", "");
				var fixedUrl = noCache.Replace("/128x128", "");
				result = "" + result.Replace(url, fixedUrl);
			}
		}
		return result;
	}
	private bool SaveNewsAndPageHtml(List<NewsItem> news, string pageHtml)
	{
		try
		{
			WriteFile(pageHtml, _cachedPage); 
			WriteFile(JsonConvert.SerializeObject(news), _newsCache);
			return true;
		}
		catch
		{
			return false;
		}
	}
	protected void Session_Start(object sender, EventArgs e)
	{
	}

	protected void Application_BeginRequest(object sender, EventArgs e)
	{
		
	}

	protected void Application_AuthenticateRequest(object sender, EventArgs e)
	{

	}

	protected void Application_Error(object sender, EventArgs e)
	{
		Trace.TraceError("Ann error occured while app execution. Application_Error. EventArgs:{0}", e.ToString());
	}

	protected void Session_End(object sender, EventArgs e)
	{

	}

	protected void Application_End(object sender, EventArgs e)
	{
		Trace.TraceInformation("Application end. EventArgs{0}", e.ToString());
	}
	public  void WriteFile(string strText, string path)
	{
		StreamWriter strWriter = new StreamWriter(path, false);
		strWriter.WriteLine(strText);
		strWriter.Close();
	}
</script>
