﻿
namespace ForexWebApp
{
	public class NewsItem
	{
		public string Date { get; set; }
		public string Title { get; set; }
		public string Url { get; set; }

		public string Content { get; set; }
		public override string ToString()
		{
			return string.Format("{0} {1}\n{2}", Date, Title, Url);
		}
	}
}
